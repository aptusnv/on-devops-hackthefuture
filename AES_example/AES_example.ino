#include <M5Stack.h>
#include "mbedtls/aes.h"

mbedtls_aes_context aes;

char result[32];
char input[18];
uint8_t output[16];
uint8_t key[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

void setup() {
  M5.begin();
  M5.Power.begin();
  M5.Lcd.println("Aptus AES ECB example");

  Serial.begin(115200);

  String ble = "00:00:00:00:00:00";
  ble.toCharArray(input, 18);

  mbedtls_aes_context aes;

  mbedtls_aes_init(&aes);
  mbedtls_aes_setkey_enc(&aes, key, 128);
  mbedtls_aes_crypt_ecb(&aes, MBEDTLS_AES_ENCRYPT, (const unsigned char*)input, output);
  mbedtls_aes_free(&aes);

  sprintf(result, "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x", output[0], output[1], output[2], output[3], output[4], output[5], output[6], output[7], output[8], output[9], output[10], output[11], output[12], output[13], output[14], output[15]);
  
  M5.Lcd.println(result);
  Serial.println(result);
}

void loop() {

}

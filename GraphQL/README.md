# The Central Computer (http://htf.aptus.be/graphql)
The central computer doesn't simply use a REST API, it uses a special API called a GraphQL API.

On a GraphQL you have to most of the work yourself. It works just like a programming language you have to call certain functions.
The weird thing is, it only accepts POST requests.

This central computer has 3 functions:
## level1
This level is very simple, you just have to pass a single parameter called code. Pass the code to the server and a pincode will be returned.

## level2
This level is secured. It takes two parameters. The first parameter is encrypted. Encrypt the code on your device and send it to the server. A pincode will be returned. You can find the value for the seconde parameter (called accessCode) in the first safe.

## level3
This is the last level. This level takes three parameters. As always, one parameter called code (unencrypted) and a second parameter called combination. You can find the combination on the display of the safe. You can find the value for the third parameter (called accessCode) in the second safe.  Send the code, the combination and the accessCode to the server and you will receive the last pincode. You can find the value for the seconde parameter (called accessCode) in the first safe.

## level5 (optional a.k.a. for the crazy people)
You are on your own for this one. You have to send a SHA256 hash of the minor you received form the bluetooth beacon. You also need the accessCode that you can find in the box of level 3. Good luck!

## level
There are multiple beacons emitting a signal. When you receive a signal but don't know what level to use it on, you can use this function.
The function, just like all the other functions, takes one parameter called code. You can pass any code you find to this function and it will return in which level you can use it.

# Documentation
We managed to hack and decrypt secret documents about how the API works.
You can find them [HERE](https://graphql.org/learn/).
Using social engineering we managed to get the GraphQL schemas.
They look like this:
```
    level1 (code: String!): String
    level2 (code: String!, accessCode: String!): String
    level3 (code: String!, combination: String!, accessCode: String!): String
    level5 (minorHash: String!, accessCode: String!): String
    level (code: String!): String
```

By the way, don't forget to escape your special characters in the string you use on your microcontroller!
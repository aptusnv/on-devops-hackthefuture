#include "WiFi.h"
#include <M5Stack.h>
#include "AptusBLE.h"

AptusBLE aptus;

void setup() {
  M5.begin();
  M5.Power.begin();
  M5.Lcd.println("Aptus BLE example");

  aptus.init();

  if (aptus.scan())
  {
    aptus.deinit();
    M5.Lcd.println("Found a beacon:");
    M5.Lcd.println(aptus.getAddress());
  }
  else
  {
    aptus.deinit();
    M5.Lcd.println("No beacons found");
  }
}

void loop() {

}

/*
  aptusble.cpp - Library for esp32 ble.
  Created by Marvin Dinneweth, November 5, 2019.
  Released into the public domain.
*/

#include "Arduino.h"
#include "AptusBLE.h"

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>

#define SERVICE_UUID "f2a74fc4-7625-44db-9b08-cb7e130b2029"

String currentAddress;
String bleAddress;

BLEScan* pBLEScan;
bool succes;

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      currentAddress = advertisedDevice.getAddress().toString().c_str();
      if (currentAddress.charAt(0) == '0' && currentAddress.charAt(1) == '0' && currentAddress.charAt(2) == ':' && currentAddress.charAt(3) == '0' && currentAddress.charAt(4) == '7' && advertisedDevice.getRSSI() > -60)
      {
        succes = true;
        bleAddress = currentAddress;
      }
    }
};

void AptusBLE::init()
{
  BLEDevice::init("Aptus");
  pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true);
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);
}

String AptusBLE::getAddress()
{
  return bleAddress;
}

bool AptusBLE::scan()
{
  succes = false;
  BLEScanResults foundDevices = pBLEScan->start(3, false);
  pBLEScan->clearResults();
  return succes;
}

void AptusBLE::deinit()
{
  BLEDevice::deinit(true);
}

/*
  aptusble.h - Library for esp32 ble.
  Created by Marvin Dinneweth, November 5, 2019.
  Released into the public domain.
*/

#ifndef AptusBLE_h
#define AptusBLE_h

#include "Arduino.h"

class AptusBLE
{
  public:
    void init();
    bool scan();
    void deinit();
    String getAddress();
};

#endif

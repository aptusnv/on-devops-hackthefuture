# HackTheFuture

We acquired a big pile of unmarked dollar bills that we want to quietly distribute to the people. We build a system of hidden safes through this building that can be openened with our state of the art security system called SOTASHTUST!!! 

SOTASHTUST first has to scan for a Bluetooth address, once captured you can verify the address with the proper authentication to a cloud API using the wifi network. If the address corresponds with a safe you get a pin code back that can be visualized on a screen. With this pin code you can open the safe and get your price.

Luckily, not all the safes are the same. Depending on the safe, it�s more complicated to get the pin code of them.

# The assignment
### Level 1

Find the safe that you can unlock by sending his BLE Mac address to the LEVEL1 API.
The M5Stack device should read the ID and do the API call when it's in the Beacon range.
You can use the Display to show the PIN code!

### Level 2

Find the safe that unlocks by sending the encrypted Mac address to the LEVEL2 API.
The M5Stack device should encrypt the scanned BLE Mac address using AES ECB with the following secret key:  
0x6D 0x14 0xC3 0xCF 0x67 0x6C 0x91 0x48 0x93 0x4A 0xBA 0xCE 0x8E 0x47 0x0B 0x02

### Level 3

The next safe has a screen that shows a unique combination send this combination to the LEVEL3 API to unlock it!

### Level 4

Update the firmware so the device can automatically get which level the scanned Beacon belongs to. Then perform the correct call for that address on the device itself via wifi.
Be sure to show off your amazing user interface you build on the M5Stack device!

### Level 5 (Optional)

There is a BLE beacon in this room that transmit advertising data with this UUID: ffffffff-ffff-ffff-ffff-ffffffffffff
Get the minor from this advertising packet en send the sha-256 hash of it to the LEVEL5 API!

# How to stand out in this challenge
- Correctly implement scanning BLE beacons
- Implement the GraphQL call via WIFI on the device itself
- Try to use less lines of code than your opponents
- Build an awesome interface for the device

# Installation for the M5Stack module

Follow the guide to install arduino for your platform here:
https://docs.m5stack.com/#/en/quick_start/m5core/m5stack_core_quick_start

To run the examples you must select the "No OTA (Large APP)" partition scheme in arduino.  
You can find this under tools -> Partition scheme

Location of SOTASHTUST:
http://htf.aptus.be/graphql
